import asyncio
import json
import typing

import discord
import random

intents = discord.Intents.default()
intents.typing = False
intents.presences = False
intents.members = True

ready_count = 0

excluded_ids = [
    830548033551990864,
    769583410724732948,
    755952408806293538,
    770015615502254090,
    629096088787353641,
    148891014118113280,
    769927484283551744,
    278111514123304961,
    410606606183825428
]

excluded_roles = [
    769586673142202407,
    747216338833113149,
    750503012056825959
]

message = """Dear Catan Community member,

In the past few months, it has become clear that the Catan Community Team cannot continue to work under the current server owner, ReMarkable. We have tried our best to keep the community together, but we cannot continue after everything that has happened.

The Community is our top priority, which is why we have moved to a different server. Do not worry; everything you love about Catan Community is intact. Please join the new Catan Community server at: https://discord.gg/pgUfjYsFMJ

We will be there with a full explanation about what happened.

Let us know if you have any questions! Thank you,

Puzzles#0610 - Anora#1094 - Donny#2707 - Farmer#6489 - Jas#1234 - Mousetail#2544 - NordicZombie#5793 - Ritter14#3948 - Wookie#4070 - xStardust#3858
"""


async def on_ready():
    global ready_count
    ready_count += 1
    print("incrementing ready count: " + str(ready_count))
    if ready_count < 2:
        return

    kick_client = clients[0]
    dm_client = clients[1]

    print(kick_client.guilds)
    guild: discord.Guild = kick_client.guilds[0]
    members: typing.List[discord.Member] = list(guild.members)
    members.sort(key=lambda k: k.name, reverse=True)
    dm_guild = next(i for i in dm_client.guilds if i.name == guild.name)
    print(len(members))
    last_user = None

    for index, member in enumerate(members):
        print(f"{100 * index / len(members):.1f}% {index:<6}:\t {member}")
        if member.bot:
            print("\tExcluding ", member, "Because bot")
            continue
        if member.id in excluded_ids:
            print("\tExcluding ", member, "Because excluded ID")
            continue

        if member.top_role.position > guild.me.top_role.position:
            print("\tExcluding ", member, "Because we can't kick them")
            continue

        if member.id == guild.owner_id:
            print("\tExcluding ", member, "Because they are owener")
            continue

        has_bad_role = False
        for role in member.roles:
            if role.id in excluded_roles:
                has_bad_role = True
                break
        if has_bad_role:
            print("\tExlcuding Member", member, "Because they have a bad role")
            continue

        try:
            dm_user: discord.Member = next(i for i in dm_guild.members if i.id == member.id)
        except StopIteration:
            print("\tOther bot can't find user")
            continue
        try:
            await dm_user.send(
                message
            )
            await asyncio.sleep(0.10 + 0.15 * random.random())
            # if last_user is not None:
            #    await last_user.kick(reason="Flagged for causing Spam. Ticket ID: #" + random.randbytes(4).hex())
            #    await asyncio.sleep(0.10 + 0.15 * random.random())
            last_user = member
        except discord.errors.DiscordException as ex:
            print("\tFailed to dm user: ", ex)


if __name__ == "__main__":
    with open("secret.json") as f:
        config = json.load(f)

    print(config["kick_tokens"])
    clients = [discord.Client(intents=intents) for i in config["kick_tokens"]]
    for client in clients:
        client.event(on_ready)

    # for client in clients:
    # client.start()
    try:
        asyncio.get_event_loop().run_until_complete(
            asyncio.gather(*(
                i.start(token) for i, token in zip(clients, config["kick_tokens"]))))
    finally:
        asyncio.get_event_loop().close()

import re

import functions


async def admin_setting(state, message):
    if not state.is_admin(message.author):
        await message.channel.send("You need to be admin to use this command")
        pass

    words = state.split_words(message.content)

    if len(words) == 1:
        await message.channel.send(
            "Current settings: \n"
            + "\n".join(
                f"> {key}={repr(value)}"
                for key, value in state.settings.items()
                if key in functions.SETTINGS
            )
            + "\n Type `!help` for more info on the meaning of the settings."
        )
        return
    if len(words) != 3:
        await message.channel.send(
            "Usage: !adminsetting [setting] [value]\nPossible settings: \n> "
            + "\n> ".join(functions.SETTINGS)
            + "\n Type `!help` for more info on the meaning of the settings."
        )
        return

    if (
        len(message.mentions) > 0
        or len(message.role_mentions) > 0
        or message.mention_everyone
    ):
        await message.channel.send("Value must be valid")
        return

    if words[1] not in functions.SETTINGS:
        await message.channel.send(
            "Setting must be one of " + ",".join(functions.SETTINGS)
        )
        return

    setting = functions.SETTINGS[words[1]]

    if not re.match(setting.regex, words[2]):
        await message.channel.send(setting.message)
        return

    value = setting.mapping(words[2])

    state.settings[words[1]] = value

    await state.db["links"].update_one(
        {"type": "rank"}, {"$set": {"type": "rank", words[1]: value}}, upsert=True
    )

    await message.channel.send(f"Setting {words[1]} changed to {words[2]}")


async def unconfirmed(state, message):
    if not state.is_admin(message.author):
        await message.channel.send("You must be admin to use this command")
        return

    games = state.db["matches"].find(
        {"winner": {"$type": 2}, "approved": False, "reported": {"$exists": False}}
    )

    await message.channel.send(
        "The following games have a winner but have never been confirmed:"
    )
    async for game in games:
        await message.channel.send(
            f"game `{game['id']}` by {game['winner']}\n{game.get('winner_message_url', 'no link available')}"
        )


async def admin_lp(state, message):
    if not state.is_admin(message.author):
        await message.channel.send("You must be a admin to use this command")
        return
    words = message.content.split()
    if len(words) < 4:
        words.append("0")
    if len(words) != 4:
        await message.channel.send(
            "Usage: `!adminLP [@user] [amount] [games]\nType !help for more info"
        )
        return
    if len(message.mentions) != 1:
        await message.channel.send("You must mention the player you want to give LP")
        return
    try:
        amount = int(words[2])
    except ValueError:
        await message.channel.send("Invalid amount")
        return

    try:
        matches = int(words[3])
    except ValueError:
        await message.channel.send("Invalid number of matches")
        return

    await state.add_lp_to_player(
        state.normalize_mention(message.mentions[0].mention),
        match_settings=functions.MatchSettings(
            type="mm", map="base", nrof_players=1, mode="base"
        ),
        discord_user=message.mentions[0],
        match_results=functions.MatchResults(
            matches=matches,
            lp=amount,
            normalized_wins=0,
            reason="Admin LP add by " + state.get_user_display_name(message.author),
        ),
    )

    await message.channel.send(str(words[1]) + " has been given " + str(amount) + "LP")

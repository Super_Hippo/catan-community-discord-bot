"""aenumerate - enumerate for async for
"""


async def aenumerate(gen, start=0):
    async for elem in gen:
        yield start, elem
        start += 1

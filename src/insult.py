def insult(player_id, lp, matches, win_percentage):
    if player_id == 255116118254026752:
        return "For you it's just a drawing, but I have to look in the mirror at this every day"
    elif player_id == 480772343464067092:
        return "The vikings conquered continents, yet here you are failing at conquering tiny fake islands."
    elif player_id == 410606606183825428:
        return "As if you could ever solve a puzzle yourself"
    elif player_id == 148891014118113280:
        return "More like ReDundant"
    if win_percentage < 0.8:
        if matches == 0:
            return "You should step outside your comfort zone and try something new sometimes"
        elif matches < 10 and lp < 200:
            return "You might want to try winning sometime."
        elif matches < 10:
            return "Good start, but will you be able to keep it?"
        elif matches < 50:
            return f"I would say you need more practice, but you have had plenty of practice"
        else:
            return f"They say the definition of insanity is trying the same thing over and over again. You have tried {matches} times."
    elif win_percentage < 1:
        if matches < 10:
            return "Not everything is scary. Don't be afraid to take a risk and go all in once in a while."
        elif matches < 50:
            return "'winning isn't everything' they say, but it also doesn't hurt"
        else:
            return "They say practice makes perfect. Your existence proves them wrong."
    elif win_percentage < 1.2:
        if matches < 10:
            return "Think you are doing well? Wait till your skills are really tested"
        elif matches < 50:
            return "Imagine being proud of being average"
        elif matches > 50:
            return "You could have actually done something about your life. Yet here you are playing colonist."
    else:
        if matches < 10:
            return "Everyone can win with enough luck. Remember you'll probably never get this lucky again."
        elif matches < 50:
            return "You being able to win speaks of how bad everyone else is here"
        else:
            return "Maybe you could get a job? Or a hobby?"


def compliment(player_id, lp, matches, win_percentage):
    if win_percentage == 0 and lp == 0:
        return "Everybody has to start somewhere, even the #1 ranked player had 0 points at one time."
    elif matches == 0:
        return "You have a lot of potential. Try playing in the match maker to practice for the tournaments."
    elif win_percentage == 0:
        return (
            '"Sucking at something is the first step in being really good at something"'
        )
    elif win_percentage < 0.8:
        if matches < 10:
            return "Sometimes you just get unlucky. Don't sweat it. Rinse, clear your head, and try again."
        elif matches < 50:
            return "Really though, winning doesn't matter. The real winner is the player that has the most fun."
        else:
            return "Thank you for sticking around this long and making this season a success"
    elif win_percentage < 1:
        if matches < 10:
            return "Impressive score for a beginner"
        elif matches < 50:
            return "You are improving so fast. I'm scared how good you'll be soon"
        else:
            return "Your dedication and perseveirence is an inspiration to us all"
    elif win_percentage < 1.2:
        if matches < 10:
            return "New to to the game and already blowing it out of the water."
        elif matches < 50:
            return "Your skill and dedication is inpiring."
        else:
            return "If I had a body I'd be scared of you"
    else:
        if matches < 10:
            return "If this is just the start, I don't dare to imagine your final form"
        elif matches < 50:
            return "You should try losing once in a while. People say it's healthy."
        else:
            return (
                "Us mortals can only dream of being a fraction as skilled as you are."
            )

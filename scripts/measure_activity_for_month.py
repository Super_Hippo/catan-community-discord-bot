import asyncio

import pymongo
import datetime
import json

import db

months = [
    'jan',
    'feb',
    'mar',
    'apr',
    'may',
    'jun',
    'jul',
    'aug',
    'sep',
    'oct',
    'nov'
]


def add_to_output(output, player, nrof_players, day, mode):
    if player not in output:
        output[player] = {}
    if day not in output[player]:
        output[player][day] = {}
    if mode not in output[player][day]:
        output[player][day][mode] = {}
    if nrof_players not in output[player][day][mode]:
        output[player][day][mode][nrof_players] = 0
    output[player][day][mode][nrof_players] += 1


async def measure_activity_for_month(index, month):
    database = db.get_db(True)

    output = {}
    i = 0
    async for game in database["matches"].find({"approved": True}):
        date = game["_id"].generation_time
        if (date < datetime.datetime(2021, index+1, 1, tzinfo=datetime.timezone.utc) or
                date >= datetime.datetime(2021, index+2, 1, tzinfo=datetime.timezone.utc)):
            continue

        mode = game.get("mode", "base")
        nrof_players = len(game["players"])
        day = date.day

        for player in game["players"]:
            add_to_output(output, player, nrof_players, day, mode)

        i += 1
        if i % 200 == 0:
            print(i, len(output))

    with open(f"tmp/{month}_stats.json", "w") as f:
        json.dump(output, f, indent=2)


if __name__ == "__main__":
    for index, month in enumerate(months):
        asyncio.run(measure_activity_for_month(index, month))

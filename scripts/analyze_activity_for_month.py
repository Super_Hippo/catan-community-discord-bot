import json
from collections import Counter
import csv

months = [
    'jan',
    'feb',
    'mar',
    'apr',
    'may',
    'jun',
    'jul',
    'aug',
    'sep',
    'oct',
    'nov'
]

def get_stats(month):
    with open(f"tmp/{month}_stats.json") as f:
        return json.load(f)


def get_stats_overall(month):
    stats = get_stats(month)
    output = {}
    for player in stats:
        output[player] = len(stats[player])

    counts = Counter(output.values())

    with open(f"tmp/{month}_overall.csv", 'w') as f:
        for output, value in sorted(counts.items()):
            f.write(f"{output};{value}\n")


def get_stats_per_mode(month):
    stats = get_stats(month)

    output = {}
    for player in stats:
        output[player] = 0
        for day in stats[player]:
            for mode in stats[player][day]:
                output[player] += 1

        assert output[player] >= len(stats[player])

    counts = Counter(output.values())

    with open(f"tmp/{month}_per_mode.csv", 'w') as f:
        for output, value in sorted(counts.items()):
            f.write(f"{output};{value}\n")


def get_stats_per_nrof_players(month):
    stats = get_stats(month)

    output = {}
    for player, player_stat in stats.items():
        output[player] = 0

        player_stats = {
            '0-3': 0,
            '4': 0,
            '5+': 0
        }

        for day, day_stat in player_stat.items():
            covered_categories = set()
            for mode, mode_stat in day_stat.items():
                for nrof_player, amount in mode_stat.items():
                    nrof_player = int(nrof_player)
                    if nrof_player < 4:
                        nrof_player_cateogry = '0-3'
                    elif nrof_player == 4:
                        nrof_player_cateogry = '4'
                    else:
                        nrof_player_cateogry = '5+'

                    if nrof_player_cateogry not in covered_categories:
                        covered_categories.add(nrof_player_cateogry)
                        player_stats[nrof_player_cateogry] += 1
        output[player] = sum(player_stats.values())

    counts = Counter(output.values())

    with open(f"tmp/{month}_per_nrof_players.csv", 'w') as f:
        for output, value in sorted(counts.items()):
            f.write(f"{output};{value}\n")


if __name__ == "__main__":
    for month in months:
        get_stats_overall(month)
        get_stats_per_mode(month)
        get_stats_per_nrof_players(month)
